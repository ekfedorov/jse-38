package ru.ekfedorov.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.model.Session;

import java.util.Optional;

public interface ISessionRepository extends IRepository<Session> {
}
