package ru.ekfedorov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.repository.IProjectRepository;
import ru.ekfedorov.tm.api.repository.ITaskRepository;
import ru.ekfedorov.tm.api.service.IConnectionService;
import ru.ekfedorov.tm.api.service.IProjectTaskService;
import ru.ekfedorov.tm.exception.empty.ProjectIdIsEmptyException;
import ru.ekfedorov.tm.exception.empty.TaskIdIsEmptyException;
import ru.ekfedorov.tm.exception.empty.UserIdIsEmptyException;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.model.Task;
import ru.ekfedorov.tm.repository.ProjectRepository;
import ru.ekfedorov.tm.repository.TaskRepository;

import static ru.ekfedorov.tm.util.ValidateUtil.*;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Optional<Task> bindTaskByProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(projectId)) throw new ProjectIdIsEmptyException();
        if (isEmpty(taskId)) throw new TaskIdIsEmptyException();
        final Connection connection = connectionService.getConnection();
        try {
            final ITaskRepository taskRepository = new TaskRepository(connection);
            @Nullable Optional<Task> task = taskRepository.bindTaskByProjectId(userId, projectId, taskId);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<Task> findAllByProjectId(
            @Nullable final String userId, @Nullable final String projectId
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(projectId)) throw new ProjectIdIsEmptyException();
        final Connection connection = connectionService.getConnection();
        final ITaskRepository taskRepository = new TaskRepository(connection);
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @SneakyThrows
    @Override
    public boolean removeProjectById(
            @Nullable final String userId, @Nullable final String projectId
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(projectId)) throw new ProjectIdIsEmptyException();
        final Connection connection = connectionService.getConnection();
        try {
            final ITaskRepository taskRepository = new TaskRepository(connection);
            final IProjectRepository projectRepository = new ProjectRepository(connection);
            taskRepository.removeAllByProjectId(userId, projectId);
            final boolean isRemove = projectRepository.removeOneById(userId, projectId);
            connection.commit();
            return isRemove;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Optional<Task> unbindTaskFromProject(
            @Nullable final String userId, @Nullable final String taskId
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(taskId)) throw new TaskIdIsEmptyException();
        final Connection connection = connectionService.getConnection();
        try {
            final ITaskRepository taskRepository = new TaskRepository(connection);
            return taskRepository.unbindTaskFromProjectId(userId, taskId);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
