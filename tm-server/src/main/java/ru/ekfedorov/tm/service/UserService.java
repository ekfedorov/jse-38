package ru.ekfedorov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.repository.IUserRepository;
import ru.ekfedorov.tm.api.service.IConnectionService;
import ru.ekfedorov.tm.api.service.IPropertyService;
import ru.ekfedorov.tm.api.service.IUserService;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.model.User;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.exception.system.LoginExistException;
import ru.ekfedorov.tm.exception.empty.*;
import ru.ekfedorov.tm.repository.UserRepository;
import ru.ekfedorov.tm.util.HashUtil;

import java.sql.Connection;
import java.util.Optional;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull IPropertyService propertyService,
            @NotNull IConnectionService connectionService) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    public IUserRepository getRepository(@NotNull Connection connection) {
        return new UserRepository(connection);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User create(
            @Nullable final String login, @Nullable final String password
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return add(user);
    }

    @SneakyThrows
    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        if (isEmpty(email)) throw new EmailIsEmptyException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        add(user);
    }

    @SneakyThrows
    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        if (role == null) throw new RoleIsEmptyException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        add(user);
    }

    @SneakyThrows
    @NotNull
    @Override
    public Optional<User> findByLogin(
            @Nullable final String login
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = new UserRepository(connection);
        return userRepository.findByLogin(login);
    }



    @SneakyThrows
    @Override
    public boolean isLoginExist(
            @Nullable final String login
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = new UserRepository(connection);
        return userRepository.isLoginExist(login);
    }

    @SneakyThrows
    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        @NotNull final Optional<User> userOptional = findByLogin(login);
        if (!userOptional.isPresent()) throw new UserNotFoundException();
        @NotNull final User user = userOptional.get();
        user.setLock(true);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(connection);
            userRepository.lockUnlockUser(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeByLogin(
            @Nullable final String login
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(connection);
            userRepository.removeByLogin(login);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void setPassword(
            @Nullable final String userId, @Nullable final String password
    ) {
        if (isEmpty(userId)) throw new IdIsEmptyException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        @NotNull final Optional<User> user = findOneById(userId);
        if (!user.isPresent()) return;
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) return;
        user.get().setPasswordHash(hash);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(connection);
            userRepository.setPassword(hash, userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        @NotNull final Optional<User> userOptional = findByLogin(login);
        if (!userOptional.isPresent()) throw new UserNotFoundException();
        @NotNull final User user = userOptional.get();
        user.setLock(false);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(connection);
            userRepository.lockUnlockUser(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (isEmpty(userId)) throw new IdIsEmptyException();
        @NotNull final Optional<User> user = findOneById(userId);
        if (!user.isPresent()) throw new NullObjectException();
        user.get().setFirstName(firstName);
        user.get().setLastName(lastName);
        user.get().setMiddleName(middleName);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(connection);
            userRepository.updateUser(user.get());
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
