package ru.ekfedorov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.ekfedorov.tm.api.repository.IProjectRepository;
import ru.ekfedorov.tm.api.service.IConnectionService;
import ru.ekfedorov.tm.api.service.IPropertyService;
import ru.ekfedorov.tm.marker.DBCategory;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.model.User;
import ru.ekfedorov.tm.service.ConnectionService;
import ru.ekfedorov.tm.service.PropertyService;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public class ProjectRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    final Connection connection = connectionService.getConnection();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository(connection);

    @After
    @SneakyThrows
    public void after() {
        connection.commit();
    }

    @Test
    @Category(DBCategory.class)
    public void addAllTest() {
        final List<Project> projects = new ArrayList<>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        projects.add(project1);
        projects.add(project2);
        projectRepository.addAll(projects);
        Assert.assertTrue(projectRepository.findOneById(project1.getId()).isPresent());
        Assert.assertTrue(projectRepository.findOneById(project2.getId()).isPresent());
    }

    @Test
    @Category(DBCategory.class)
    public void addTest() {
        final Project project = new Project();
        Assert.assertNotNull(projectRepository.add(project));
    }

    @Test
    @Category(DBCategory.class)
    public void clearTest() {
        projectRepository.clear();
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void containsTest() {
        final Project project1 = new Project();
        final String projectId = project1.getId();
        projectRepository.add(project1);
        Assert.assertTrue(projectRepository.contains(projectId));
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void findAll() {
        final int projectSize = projectRepository.findAll().size();
        final List<Project> projects = new ArrayList<>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        projects.add(project1);
        projects.add(project2);
        projectRepository.addAll(projects);
        Assert.assertEquals(2 + projectSize, projectRepository.findAll().size());
        projectRepository.remove(project1);
        projectRepository.remove(project2);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllSortByUserId() {
        final List<Project> projects = new ArrayList<>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        final Project project3 = new Project();
        final User user = new User();
        final String userId = user.getId();
        project1.setName("b");
        project2.setName("c");
        project3.setName("a");
        project1.setUserId(userId);
        project2.setUserId(userId);
        project3.setUserId(userId);
        projects.add(project1);
        projects.add(project2);
        projects.add(project3);
        projectRepository.addAll(projects);
        final String sort = "NAME";
        final List<Project> projects2 = new ArrayList<>(projectRepository.findAll(userId, sort));
        Assert.assertFalse(projects2.isEmpty());
        Assert.assertEquals(3, projects2.size());
        Assert.assertEquals("a", projects2.get(0).getName());
        Assert.assertEquals("b", projects2.get(1).getName());
        Assert.assertEquals("c", projects2.get(2).getName());
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIdTest() {
        final Project project1 = new Project();
        final String projectId = project1.getId();
        projectRepository.add(project1);
        Assert.assertNotNull(projectRepository.findOneById(projectId));
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTest() {
        final Project project = new Project();
        projectRepository.add(project);
        final String projectId = project.getId();
        Assert.assertTrue(projectRepository.findOneById(projectId).isPresent());
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTestByUserId() {
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        projectRepository.add(project);
        final String projectId = project.getId();
        Assert.assertTrue(projectRepository.findOneById(userId, projectId).isPresent());
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByNameTest() {
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        project.setName("pr1");
        projectRepository.add(project);
        final String name = project.getName();
        Assert.assertNotNull(name);
        Assert.assertTrue(projectRepository.findOneByName(userId, name).isPresent());
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void removeOneByIdTest() {
        final Project project = new Project();
        projectRepository.add(project);
        final String projectId = project.getId();
        projectRepository.removeOneById(projectId);
        Assert.assertFalse(projectRepository.findOneById(projectId).isPresent());
        projectRepository.remove(project);
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIdTestByUserId() {
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        projectRepository.add(project);
        final String projectId = project.getId();
        Assert.assertTrue(projectRepository.removeOneById(userId, projectId));
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIndexTest() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        final Project project3 = new Project();
        final User user = new User();
        final String userId = user.getId();
        project1.setUserId(userId);
        project2.setUserId(userId);
        project3.setUserId(userId);
        projectRepository.add(project1);
        projectRepository.add(project2);
        projectRepository.add(project3);
        Assert.assertTrue(projectRepository.findOneByIndex(userId, 0).isPresent());
        Assert.assertTrue(projectRepository.findOneByIndex(userId, 1).isPresent());
        Assert.assertTrue(projectRepository.findOneByIndex(userId, 2).isPresent());
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByNameTest() {
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        project.setName("pr1");
        projectRepository.add(project);
        final String name = project.getName();
        Assert.assertNotNull(name);
        Assert.assertTrue(projectRepository.removeOneByName(userId, name));
    }

    @Test
    @Category(DBCategory.class)
    public void removeTest() {
        final Project project = new Project();
        projectRepository.add(project);
        Assert.assertNotNull(projectRepository.remove(project));
    }

    @Test
    @Category(DBCategory.class)
    public void removeTestByUserIdAndObject() {
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        projectRepository.add(project);
        Assert.assertTrue(projectRepository.remove(userId, project));
    }

}
