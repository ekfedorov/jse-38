package ru.ekfedorov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.ekfedorov.tm.api.repository.ISessionRepository;
import ru.ekfedorov.tm.api.service.IConnectionService;
import ru.ekfedorov.tm.api.service.IPropertyService;
import ru.ekfedorov.tm.marker.DBCategory;
import ru.ekfedorov.tm.model.Session;
import ru.ekfedorov.tm.service.ConnectionService;
import ru.ekfedorov.tm.service.PropertyService;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public class SessionRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    final Connection connection = connectionService.getConnection();

    @NotNull
    final ISessionRepository sessionRepository = new SessionRepository(connection);

    @After
    @SneakyThrows
    public void after() {
        connection.commit();
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void addAllTest() {
        final List<Session> users = new ArrayList<>();
        final Session user1 = new Session();
        final Session user2 = new Session();
        users.add(user1);
        users.add(user2);
        sessionRepository.addAll(users);
        Assert.assertTrue(sessionRepository.findOneById(user1.getId()).isPresent());
        Assert.assertTrue(sessionRepository.findOneById(user2.getId()).isPresent());
        sessionRepository.remove(user1);
        sessionRepository.remove(user2);
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void addTest() {
        final Session session = new Session();
        Assert.assertNotNull(sessionRepository.add(session));
        sessionRepository.remove(session);
    }

    @Test
    @Category(DBCategory.class)
    public void clearTest() {
        sessionRepository.clear();
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void containsTest() {
        final Session Session1 = new Session();
        final String SessionId = Session1.getId();
        sessionRepository.add(Session1);
        Assert.assertTrue(sessionRepository.contains(SessionId));
        sessionRepository.remove(Session1);
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void findAll() {
        final int SessionSize = sessionRepository.findAll().size();
        final List<Session> session = new ArrayList<>();
        final Session Session1 = new Session();
        final Session Session2 = new Session();
        session.add(Session1);
        session.add(Session2);
        sessionRepository.addAll(session);
        Assert.assertEquals(2 + SessionSize, sessionRepository.findAll().size());
        sessionRepository.remove(Session1);
        sessionRepository.remove(Session2);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIdTest() {
        final Session session = new Session();
        final String sessionId = session.getId();
        sessionRepository.add(session);
        Assert.assertNotNull(sessionRepository.findOneById(sessionId));
        sessionRepository.remove(session);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTest() {
        final Session session = new Session();
        sessionRepository.add(session);
        final String sessionId = session.getId();
        Assert.assertTrue(sessionRepository.findOneById(sessionId).isPresent());
        sessionRepository.remove(session);
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void removeOneByIdTest() {
        final Session session = new Session();
        sessionRepository.add(session);
        final String sessionId = session.getId();
        sessionRepository.removeOneById(sessionId);
        Assert.assertFalse(sessionRepository.findOneById(sessionId).isPresent());
        sessionRepository.remove(session);
    }

    @Test
    @Category(DBCategory.class)
    public void removeTest() {
        final Session session = new Session();
        sessionRepository.add(session);
        Assert.assertNotNull(sessionRepository.remove(session));
        sessionRepository.remove(session);
    }

}
